<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = "student";

    public $primarykey="id";

    protected  $guarded = ['id'];
}
