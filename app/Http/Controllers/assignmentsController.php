<?php


namespace App\Http\Controllers;

use App\assignments;
use Illuminate\Http\Request;

class assignmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assignments = assignments::all();

        return view('assignments.assignments',compact('assignments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //dd('1');
        return view('assignments.create')->with('alert', 'Assignment Added Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Assignment = new assignments();

        $Assignment->sid = $request->get('sid');
        $Assignment->sname = $request->get('sname');
        $Assignment->lecturer = $request->get('lecturer');
        $Assignment->file = $request->get('file');

        $Assignment->save();

        return redirect('/assignments/show')->with('alert', 'Assignment Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $assignments = assignments::all();

        return view('assignments.show',compact('assignments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\assignments  $player
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $assignments = assignments::findOrFail($id);
        return view('assignments.edit',compact('assignments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\assignments  $player
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd('grttt');

        $assignments = assignments::findOrFail($id);

        $assignments->sid = $request->get('sid');
        $assignments->sname = $request->get('sname');
        $assignments->lecturer = $request->get('lecturer');
        $assignments->file = $request->get('file');

        $assignments->save();

        return redirect('/assignments/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\assignments  $player
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('dele');
        $assignments = assignments::findOrFail($id);
        $assignments->delete();

        return redirect('/assignments/show');
    }
}
