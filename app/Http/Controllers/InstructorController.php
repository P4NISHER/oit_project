<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstructorData;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\session;
use DB;

class InstructorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Instructor.Instructor');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $Instructor = InstructorData::all();
        return view('Instructor.create', compact('Instructor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return DB::transaction(function() use($request){

            $contact = new InstructorData([
           
           
            'firstname' => $request->get('firstname'),
            'lastname'  => $request->get('lastname'),
            'address'   => $request->get('address'),
            'email'     => $request->get('email'),
            'phoneNo'   => $request->get('phoneNo'),
            'subjects'  => $request->get('subjects')

            ]);

            $contact->save();
    
            return redirect('/instructor/show');

        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $instructor = InstructorData::all();

        return view('Instructor.show',compact('instructor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Instructor = InstructorData::findOrFail($id);
        return view('Instructor.edit',compact('Instructor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Instructor = InstructorData::findOrFail($id);

        $Instructor->firstname = $request->get('firstname');
        $Instructor->lastname  = $request->get('lastname');
        $Instructor->address   = $request->get('address');
        $Instructor->email     = $request->get('email');
        $Instructor->phoneNo   = $request->get('phoneNo');
        $Instructor->subjects  = $request->get('subjects');

        $Instructor->save();
        return redirect('/instructor/show');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $Instructor = InstructorData::findOrFail($id);
        // $Instructor->delete();

        // return redirect() ->route('Instructor.show')->with('success', 'Remove Instructor Details ');

        $Instructor = InstructorData::findOrFail($id);
        $Instructor->delete();

        return redirect('/instructor/show');
    }
}
