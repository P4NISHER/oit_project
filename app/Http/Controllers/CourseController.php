<?php

namespace App\Http\Controllers;

use Validator;
use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\session;
use DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('course.course');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('course.courseList',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            return DB::transaction(function () use($request){

                $validatedData = $request->validate([
                    'coursename' => ['required', 'max:50'],
                    'coursecode' => ['required', 'max:6'],
                    'enroll' => ['required'],
                    'inLect' => ['required'],
                ]);

                $courses = new Course([
                    'coursename' => $request->get('coursename'),
                    'coursecode' => $request->get('coursecode'),
                    'enroll' => $request->get('enroll'),
                    'inLect' => $request->get('inLect')
                ]);

            $courses->save();

            return redirect()->route('course.create')->with('message', 'Course adding successfull..!');
    });

    }

    /**
     * Display the all resources.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // $courses = Course::findOrFail($id);
        //return view('course.courseHome');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courses = Course::findOrFail($id);
         return view('course.courseedit', compact('courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $courses = Course::findOrFail($id);
        $courses->update($request->all());
        return redirect()->route('course.create')->with('upmssge', 'Successfully Updated..!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courses = Course::findOrFail($id);
        $courses -> delete();
        return redirect()->route('course.create')->with('delmssge', 'Successfully deleted..!');
        
    }
}
