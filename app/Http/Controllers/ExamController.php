<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\exam;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    
        return view('exam.exam');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('exam.exam');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $Exam = new exam();

        $Exam->subName = $request->get('subName');
        $Exam->subId = $request->get('subId');
        $Exam->date = $request->get('date');
        $Exam->time = $request->get('time');
        $Exam->venue = $request->get('venue');
   
        $Exam->save();

        return redirect('/exam');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $exam = exam::all();

        return view('exam.showExam',compact('exam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $exam = exam::findOrFail($id);
        return view('exam.editExam',compact('exam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $exam = exam::findOrFail($id);

        $exam->subName = $request->get('subName');
        $exam->subId = $request->get('subId');
        $exam->date = $request->get('date');
        $exam->time = $request->get('time');
        $exam->venue = $request->get('venue');

        $exam->save();

        return redirect('/exam/showExam');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $exam = exam::findOrFail($id);
        $exam->delete();

        return redirect('/exam/showExam');
    }
}
