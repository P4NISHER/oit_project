<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use DB;

class studentController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::all();

        return view('Student.student',compact('student'));
    }

    /**
     * Show the form for creating a new resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Student.create')->with('alert', 'Added Successfully');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      return DB::transaction(function() use($request){

            $contact = new Student([
           
           
            'firstname' => $request->get('firstname'),
            'lastname'  => $request->get('lastname'),
            'address'   => $request->get('address'),
            'email'     => $request->get('email'),
            'phoneNo'   => $request->get('phoneNo')
            

            ]);

            $contact->save();
    
            return redirect('/student/show')->with('alert', ' Added Successfully');
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::all();
        return view('Student.show',compact('student'));
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $student = Student::findOrFail($id);
        return view('Student.edit',compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $student = Student::findOrFail($id);

        $student->firstname = $request->get('firstname');
        $student->lastname = $request->get('lastname');
        $student->address = $request->get('address');
        $student->email = $request->get('email');
        $student->phoneNo = $request->get('phoneNo');

        $student->save();

        return redirect('/student/show');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $student = student::findOrFail($id);
        $student->delete();

        return redirect('/student/show');
    }
}
