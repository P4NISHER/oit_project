<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstructorData extends Model
{
    protected $table = "instructor";

    public $primarykey="id";

    protected  $guarded = ['id'];
}
