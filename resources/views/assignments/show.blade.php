@extends('layout')

@section('content')

    <!DOCTYPE html>
<html lang="">
<head>
    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <style>
        a:link, a:visited {
            background-color: #008CBA;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: red;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <title></title>
</head>
<br style="margin-left: 400px; margin-right: 400px; margin-top: 100px">

<br>

<h1 class="title" style="text-align: center"> Below is the details of Assignments </h1>

@foreach ($assignments as $Assignment)

<table>

    <tr>
        <td>Assignment ID: {{ $Assignment->id}}</td>
    </tr>
    <tr>
        <td>Student ID: {{ $Assignment->sid }}</td>
    </tr>
    <tr>
        <td>Student Name: {{ $Assignment->sname }}</td>
    </tr>
    <tr>
        <td>Lecturer: {{ $Assignment->lecturer }}</td>
    </tr>
    <tr>
        <td>File Name: {{ $Assignment->file }}</td>
    </tr>
    <tr>
        <td>
            <a style=" padding: 5px;" href="{{ $Assignment->id }}/edit">Edit Assignment</a>

            <a style=" padding: 5px;" href="#" >Delete Assignment</a>

        </td>
    </tr>
</table>
<br>
<br>
<br>
@endforeach

@endsection

</body>
</html>
