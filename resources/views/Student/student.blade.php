@extends('layout')

@section('content')

<!DOCTYPE html>
<html lang="">
<head>

    <title></title>
</head>
<style>

    .container
    {
        padding-bottom: 20px;
        background-color: #ccddff;
    }


</style>
@if(session()->has('jsAlert'))
    <script>
        alert({{ session()->get('jsAlert') }});
    </script>
@endif
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="margin-left: 500px; margin-top: 100px;">
            <br>
            <h1 class="title">Student  - 2019</h1>

            <br/>
            <form action="{{ route('student.create') }}">
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-link" style="margin-left: 100px; width: 200px; height:40px">Create New Student</button>
                    </div>
                </div>
            </form>
            <br/>
            <button type="submit" class="button is-link" style="margin-left: 100px; width: 200px; height:40px">
            <a style=" color: white" href="student/show">View Details</a>
            </button>

        </div>
    </div>
</div>
</body>
</html>


@endsection
