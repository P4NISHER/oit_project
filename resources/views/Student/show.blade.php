@extends('layout')

@section('content')

    <!DOCTYPE html>
<html lang="">
<head>
    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <style>
        a:link, a:visited {
            background-color: #008CBA;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: red;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <title></title>
</head>
<br style="margin-left: 400px; margin-right: 400px; margin-top: 100px">

<br>

<h1 class="title" style="text-align: center">Details of Students </h1>

@foreach ($student as $Student)

<table>

    <tr>
        <td>Student ID: {{ $Student->id}}</td>
    </tr>
    <tr>
        <td>First Name: {{ $Student->firstname }}</td>
    </tr>
    <tr>
        <td>Last Name: {{ $Student->lastname }}</td>
    </tr>
    <tr>
        <td>Address: {{ $Student->address }}</td>
    </tr>
    <tr>
        <td>Email: {{ $Student->email }}</td>
    </tr>
    <tr>
        <td>Phone No: {{ $Student->phoneNo }}</td>
    </tr>
    <tr>
        <td>
            <a style=" padding: 5px; width:130px; height:35px" href="{{ $Student->id }}/edit">Edit Student</a>
        
           
            <form style=" margin-top: 10px" method="POST" action="{{ route('student.destroy', $Student->id) }}">

        @method('DELETE')
        @csrf


        <div class="field">

        <div class="control">

        <button style="width:130px; height:35px" type="submit" class="button is-link">Delete Student</button>

        </div>

        </div>

        </form>

        </td>
    </tr>
</table>
<br>
<br>
<br>
@endforeach

@endsection

</body>
</html>
