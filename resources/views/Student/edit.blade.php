@extends('layout')
@section('content')

    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <div class="container">

    <form style="margin-left: 400px; margin-right: 400px; margin-top: 100px" method="POST" action="{{ route('student.update', $student->id) }}">

        <h1 class="title" style="text-align: center"> Edit Student </h1>
  
        @method('PATCH')
        @csrf

        <div class="field">

            <label class="label" for="name">First Name </label>

            <div class="control">

                <input type="text" class="input" name="firstname" placeholder="" value="{{ $student->firstname }}" required>

            </div>

        </div>

        <div class="field">

            <label class="label" for="sport">Last Name </label>

            <div class="control">

                <input type="text" class="input" name="lastname" placeholder="" value="{{ $student->lastname }}" required>


            </div>

        </div>
        <div class="field">

            <label class="label" for="sport">Address</label>

            <div class="control">

                <input type="text" class="input" name="address" placeholder="" value="{{ $student->address }}" required>


            </div>

        </div>
        <div class="field">

            <label class="label" for="sport">Email </label>

            <div class="control">

                <input type="text" class="input" name="email" placeholder="" value="{{ $student->email }}" required>


            </div>

        </div>

        <div class="field">

        <label class="label" for="sport">Phone No </label>

        <div class="control">

        <input type="text" class="input" name="phoneNo" placeholder="" value="{{ $student->phoneNo }}" required>


        </div>

        </div>


        <div class="field">

            <div class="control">

                <button style="width: 100%" type="submit" class="button is-link">Update Student</button>

            </div>

        </div>

    </form>

        <form style="margin-left: 400px; margin-right: 400px; margin-top: 10px" method="POST" action="{{ route('student.destroy', $student->id) }}">

            @method('DELETE')
            @csrf


            <div class="field">

                <div class="control">

                    <button style="width: 100%" type="submit" class="button is-link">Delete Student</button>

                </div>

            </div>

        </form>

    </div>
@endsection

