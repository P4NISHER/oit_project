@extends('main')

@section('body')
<div class="row" style="background-color:lightblue;margin-top:50px;">
    <div class="col-lg-3 mx-auto" style="margin-top:100px;padding-bottom:50px;">
        <h1 style="align:center;color:black;">Course 2019</h1>
        <div>
        <a class="btn btn-primary" style="color:white; background-color:#008CBA;height:35px; width:150px;" href="{{ url('/course') }}">Add Course</a>
        </div>
        <br>
        <br>
        <div>
        <a class="btn btn-primary" style="color:white; background-color:#008CBA;height:35px; width:150px;" href="{{ route('course.create') }}">View Courses</a>
        </div>
    </div>
</div>
@endsection