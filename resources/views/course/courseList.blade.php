@extends('main')

@section('body')
<h1 class="mt-4">Details of Courses</h1>
@if (Session::has('message'))
        <div class="alert alert=success">{{Session::get('message')}}

        </div>
@endif

@if (Session::has('message'))
        <div class="alert alert=success">{{Session::get('message')}}

        </div>
@endif

@if (Session::has('upmssge'))
<div class="alert alert=success">{{Session::get('upmssge')}}

</div>
@endif

@if (Session::has('delmssge'))
        <div class="alert alert=success">{{Session::get('delmssge')}}

        </div>
@endif

<table class="table">
    <tr style="background-color: lightblue;">
        <th>Course Name</th>
        <th>Course Code</th>
        <th>Enrollment key</th>
        <th>Incharge Lecturer</th>
        <th>Edit</th>
        <th>Delete</th>
    </tr>
    @foreach ($courses as $course)
    <tr>
        
        <td>{{$course->coursename}}</td>
        <td>{{$course->coursecode}}</td>
        <td>{{$course->enroll}}</td>
        <td>{{$course->inLect}}</td>
        <td><a class="btn btn-primary" style="color:white; background-color:#008CBA; padding:5px;width:100px;" href="{{ route('course.edit', $course->id) }}">Edit</a>
        </td>
        <td>
        <form method="POST" action="{{route('course.destroy', $course->id)}}">

                @method('DELETE')
                @csrf
        
                        <button class="btn btn-danger" style="color:white; padding:5px;width:100px;" type="submit" class="button is-link">Delete</button>
    
            </form>
        
        </td>        
    </tr>
    @endforeach
</table>
<a class="btn btn-primary" id="cancel" href="{{ url('/') }}">Cancel</a>

@endsection