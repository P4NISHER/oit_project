@extends('main')

@section('body')
    
<div>
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<br>
<br>
<h2>Course Details Update</h2>

<form action="{{ route('course.update',$courses->id) }}" method ="POST">
    @method('PATCH')
    @csrf

    <label for = "CourseName">Course name: </label>
    <input type="text" class="input" name="coursename" value="{{ $courses->coursename }}"><br>

    <label for = "CourseCode">Course Code:</label>
    <input type="text" class="input" name="coursecode" value="{{ $courses->coursecode }}"><br>

    <label for = "Enrollment">Enrollment Key: </label>
    <input type="text" class="input" name="enroll" value="{{ $courses->enroll }}"><br>

    <label for = "Lectrer">Lecturer Incharge:</label>
    <input type="text" class="input" name="inLect" value="{{ $courses->inLect }}"><br>

    <input type="submit"  id ="submit" align="center" value="Update">

    <a class="btn btn-primary" id="cancel" href="{{ url('/') }}">Cancel</a>


</form>

</div>

</div>

@endsection
