
@extends('main')

@section('body')
    <div class="row">
        <div class="col-lg-6 mx-auto">
            <div>
                <br>
                <br>
                <h2>Course Registration</h2>
        
                <form action="{{ route('course.store') }}" method ="POST">
                    @csrf
        
                    <label for = "CourseName">Course name: </label>
                    <input type="text" class="form-control" name="coursename"><br>
        
                    <label for = "CourseCode">Course Code:</label>
                    <input type="text" class="form-control" name="coursecode"><br>
        
                    <label for = "Enrollment">Enrollment Key: </label>
                    <input type="text" class="form-control" name="enroll"><br>
        
                    <label for = "Lectrer">Lecturer Incharge:</label>
                    <input type="text" class="form-control" name="inLect"><br>
        
                    <input type="submit"  id ="submit" align="center" value="Submit"> 
                    <a class="btn btn-primary" id="cancel" href="{{ url('/') }}">Cancel</a>
                </form>
               
            </div>
        
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
        </div>
        </div>
    </div>
@endsection

