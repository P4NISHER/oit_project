@extends('layout')

@section('content')

    <!DOCTYPE html>
<html lang="">
<head>
    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <style>

        a:link, a:visited {
            background-color: #008CBA;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: blue;
        }

        table {
            font-family: arial, sans-serif;
            width: 50%;
            align : center;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: white;
        }
        
    </style>
    <title></title>
</head>

<br style="margin-left: 400px; margin-right: 400px; margin-top: 100px">

<br>

<h1 class="title" style="text-align: center"> Exam Details </h1>

@foreach ($exam as $exam)

<table>

    <tr>
        <td>Subject Name : {{ $exam->subName}}</td>
    </tr>
    <tr>
        <td>Subject ID : {{ $exam->subId }}</td>
    </tr>
    <tr>
        <td>Date : {{ $exam->date }}</td>
    </tr>
    <tr>
        <td>Time : {{ $exam->time }}</td>
    </tr>
    <tr>
        <td>venue : {{ $exam->venue }}</td>
    </tr>
</table>


<tr>
    <a style=" padding: 10px;" href="{{ $exam->id }}/edit">Edit Exam</a>

    <a style=" padding: 10px;" href="#" >Delete Exam</a>
</tr>

<br>
<br>
<br>
@endforeach

@endsection

</body>
</html>
