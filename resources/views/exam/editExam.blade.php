@extends('layout')
@section('content')

    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <div class="container">

    <form style="margin-left: 400px; margin-right: 400px; margin-top: 100px" method="POST" action="{{ route('exam.update', $exam->id) }}">

        <h1 class="title" style="text-align: center"> Edit Exam Details </h1>

        @method('PATCH')
        @csrf

        <div class="field">
            <label class="label" for="name">Subject Name </label>
            <div class="control">
                <input type="text" class="input" name="subName" placeholder="" value="{{ $exam->subName }}" required>
            </div>
         </div>

        <div class="field">
            <label class="label" for="sport">Subject ID </label>
            <div class="control">
                <input type="text" class="input" name="subId" placeholder="" value="{{  $exam->subId }}" required>
            </div>
        </div>

        <div class="field">
            <label class="label" for="sport">Date </label>
            <div class="control">
                <input type="text" class="input" name="date" placeholder="" value="{{ $exam->date }}" required>
            </div>
        </div>

        <div class="field">
            <label class="label" for="sport">Time </label>
            <div class="control">
                <input type="text" class="input" name="time" placeholder="" value="{{ $exam->time }}" required>
            </div>
        </div>

        <div class="field">
            <label class="label" for="sport">Venue </label>
            <div class="control">
                <input type="text" class="input" name="venue" placeholder="" value="{{ $exam->venue }}" required>
            </div>
        </div>

        <div class="field">
            <div class="control">
                <button style="width: 100%" type="submit" class="button is-link"> Update Details</button>
            </div>
        </div>
        
    </form>

        <form style="margin-left: 400px; margin-right: 400px; margin-top: 10px" method="POST" action="{{ route('exam.destroy', $exam->id) }}">

            @method('DELETE')
            @csrf

            <div class="field">
                <div class="control">
                    <button style="width: 100%" type="submit" class="button is-link">Delete Exam Details</button>
                </div>
            </div>

        </form>
    </div>
@endsection

