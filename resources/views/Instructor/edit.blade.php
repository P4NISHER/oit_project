@extends('layout')
@section('content')

    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <div class="container">

    <form style="margin-left: 400px; margin-right: 400px; margin-top: 100px" method="POST" action="{{ route('instructor.update', $Instructor->id) }}">

        <h1 class="title" style="text-align: center"> Edit Instructor </h1>

        @method('PATCH')
        @csrf

        <div class="field">

            <label class="label" for="name">Instructor ID </label>

            <div class="control">

                <input type="text" class="input" name="id" placeholder="" value="{{ $Instructor->id }}" required>

            </div>

        </div>

        <div class="field">

            <label class="label" for="sport">First Name </label>

            <div class="control">

                <input type="text" class="input" name="firstname" placeholder="" value="{{ $Instructor->firstname }}" required>


            </div>

        </div>
        <div class="field">

            <label class="label" for="sport">Last Name </label>

            <div class="control">

                <input type="text" class="input" name="lastname" placeholder="" value="{{ $Instructor->lastname }}" required>


            </div>

        </div>
        <div class="field">

            <label class="label" for="sport">Address </label>

            <div class="control">

                <input type="text" class="input" name="address" placeholder="" value="{{ $Instructor->address }}" required>


            </div>

            </div>
        <div class="field">

            <label class="label" for="sport">E-mail</label>

            <div class="control">

                <input type="text" class="input" name="email" placeholder="" value="{{ $Instructor->email }}" required>


            </div>

        </div>

         <div class="field">

            <label class="label" for="sport">Phone No </label>

            <div class="control">

                <input type="text" class="input" name="phoneNo" placeholder="" value="{{ $Instructor->phoneNo }}" required>


            </div>

        </div>

         <div class="field">

            <label class="label" for="sport">Subjects </label>

            <div class="control">

                <input type="text" class="input" name="subjects" placeholder="" value="{{ $Instructor->subjects }}" required>


            </div>
            </div>
  <form style="margin-left: 400px; margin-right: 400px; margin-top: 10px" method="POST" action="">

        <div class="field">

            <div class="control">

                <button style="width: 100%" type="submit" class="button is-link">Update Instructor</button>

            </div>

        </div>

    </form>

    <form style="margin-left: 400px; margin-right: 400px; margin-top: 10px" method="POST" action="{{ route('instructor.destroy', $Instructor->id) }}">
            @method('DELETE')
            @csrf


            <div class="field">

                <div class="control">

                    <button style="width: 100%" type="submit" class="button is-link">Remove Instructor</button>

                </div>

            </div>

        </form>

    </div>
@endsection

