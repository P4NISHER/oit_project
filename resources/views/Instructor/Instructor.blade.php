@extends('layout')

@section('content')

<!DOCTYPE html>
<html lang="">
<head>

    <title></title>
</head>
<style>

    .container
    {
        padding-bottom: 20px;
        background-color: #ccddff;
    }


</style>
@if(session()->has('jsAlert'))
    <script>
        alert({{ session()->get('jsAlert') }});
    </script>
@endif
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="margin-left: 500px; margin-top: 100px;">
            <br>
            <h1 class="title">Instructor Home</h1>

            <br/>
            <form action="{{ url('/instructor/create') }}">
                <div class="field">
                    <div class="control">
                        <button type="submit" class="button is-link" style="margin-left: 100px">Insert new Instructor</button>
                    </div>
                </div>
            </form>
            <br/>
            <form action="{{ url('/instructor/show') }}">
            <button type="submit" class="button is-link" style="margin-left: 100px; width: 195px">
            <a style=" color: white;">View Instructor Details</a>
            </button>

        </div>
    </div>
</div>
</body>
</html>


@endsection
