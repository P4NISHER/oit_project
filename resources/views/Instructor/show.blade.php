@extends('layout')

@section('content')

    <!DOCTYPE html>
<html lang="">
<head>
    @if(session()->has('jsAlert'))
        <script>
            alert({{ session()->get('jsAlert') }});
        </script>
    @endif
    <style>
        a:link, a:visited {
            background-color: #008CBA;
            color: white;
            padding: 14px 25px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
        }

        a:hover, a:active {
            background-color: red;
        }
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <title></title>
</head>
<br style="margin-left: 400px; margin-right: 400px; margin-top: 100px">

<br>

<h1 class="title" style="text-align: center"> Details of Instructor </h1>

@foreach ( $instructor as $instructor)

<table>

    <tr>
        <td>Instructor ID: {{ $instructor->id}}</td>
    </tr>
    <tr>
        <td>First Name: {{ $instructor->firstname }}</td>
    </tr>
    <tr>
        <td>Last Name: {{ $instructor->lastname }}</td>
    </tr>
    <tr>
        <td>Address: {{ $instructor->address }}</td>
    </tr>
    <tr>
        <td>E-mail: {{ $instructor->email }}</td>
    </tr>
    <tr>
        <td>Phone No: {{ $instructor->phoneNo }}</td>
    </tr>
    <tr>
        <td>Subjects: {{ $instructor->subjects }}</td>
    </tr>
    <tr>
        <td>
            <a style=" padding: 5px;" href="{{ route('instructor.edit', $instructor->id) }}">Edit Instructor</a>
       </td> 
          <br>
          <br>
          <tr>
          <td>
            <form  method="POST" action="{{ route('instructor.destroy', $instructor->id) }}">
            @method('DELETE')
            @csrf


            <div class="field">

                <div class="control">

                    <button style = " padding: 5px;" type="submit" class="button is-link">Remove Instructor</button>

                </div>

            </div>

        </form>
        </td>
        
        </tr>
        
       
   
</table>
<br>
<br>
<br>
@endforeach

@endsection

</body>
</html>
