<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('course', 'CourseController');


Route::resource('assignments','assignmentsController');

Route::resource('student', 'studentController');

Route::resource('exam', 'ExamController');

//Route::get('/student1', ['uses'=> 'studentController@index']);

Route::get('/st', function() {
    return view('Student.create');

});
    Route::get('/course_home', function () {
        return view('course.courseHome');
    });


    Route::get('/show', function () {
        return view('assignments.show');

    });

    Route::get('/st1', function () {
        return view('Student.index');
    });


    Route::resource('instructor', 'InstructorController');


// Route::get('/instructor', ['uses'=>'InstructorController@index']);

// Route::get('/createInstructor', function(){
//     return view('instructor.create');
// });

// Route::get('/createInstructor/{id}', function(){
//     return view('instructor.create');
// });



