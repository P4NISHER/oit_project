<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstructorDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instructor_data', function (Blueprint $table) {
            Schema::create('Instructor', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('firstname');
                $table->string('lastname');
                $table->string('address');
                $table->string('email');
                $table->string('phoneNo');
                $table->string('subjects');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Instructor');
    }
}
